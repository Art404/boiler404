import React from 'react'
import Logo from '../Logo/Logo'


class Navigation extends React.Component {
  static displayName = 'Navigation'

  render () {
    return (
      <nav className="Navigation">
        <div className="Navigation-logo">
          <Logo />
        </div>
      </nav>
    )
  }
}

export default Navigation
