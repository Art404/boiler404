import React from 'react'
import Helmet from "react-helmet"
import cfg from './config'
import Navigation from '../Navigation/Navigation'
import connectToStores from 'alt/utils/connectToStores'
import AppStore from '../../stores/AppStore'
import AppActions from '../../actions/AppActions'


class App extends React.Component {

  static displayName = 'App'

  static propTypes = {
    app: React.PropTypes.object,
    children: React.PropTypes.object
  }

  static getStores() {
    return [AppStore]
  }

  static getPropsFromStores() {
    return AppStore.getState()
  }

  constructor (props) {
    super(props)
  }

  render() {
    console.log('APP PROPS >>> ', this.props)
    //children are the routes
    const {children, app} = this.props

    return (
      <div className="Page">
        <Helmet {... cfg} />
        <Navigation />
        <div className="App-wrap">
          {children}
        </div>
      </div>
    )
  }
}

export default connectToStores(App)
